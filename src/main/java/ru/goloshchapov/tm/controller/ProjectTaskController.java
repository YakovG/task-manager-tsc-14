package ru.goloshchapov.tm.controller;

import ru.goloshchapov.tm.api.IProjectTaskController;
import ru.goloshchapov.tm.api.IProjectTaskService;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void createTestData() {
        System.out.println("CREATE TEST DATASET");
        projectTaskService.createTestData();
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        List<Project> projects = projectTaskService.findAllProjects();
        if (projects == null) return;
        for (final Project project: projects) {
            projectTaskService.removeAllByProjectId(project.getId());
        }
        projects.clear();
        projectTaskService.clearProjects();
        System.out.println("[OK]");
    }

    @Override
    public void showAllByProjectId() {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER PROJECT ID");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllByProjectId(id);
        if (tasks == null) {
            System.out.println("[FAIL]");
            return;
        }
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showAllByProjectName() {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        final String name = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllByProjectName(name);
        if (tasks == null) {
            System.out.println("[FAIL]");
            return;
        }
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showAllByProjectIndex() {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber() -1;
        final List<Task> tasks = projectTaskService.findAllByProjectIndex(projectIndex);
        if (tasks == null) {
            System.out.println("[FAIL]");
            return;
        }
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void addTaskToProjectByIds() {
        System.out.println("ADD TASK TO PROJECT");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindToProjectById(projectId, taskId);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskFromProjectByIds() {
        System.out.println("REMOVE TASK FROM PROJECT");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindFromProjectById(projectId, taskId);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeAllByProjectId() {
        System.out.println("[REMOVE ALL TASKS FROM PROJECT]");
        System.out.println("ENTER PROJECT ID");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.removeAllByProjectId(id);
        if (tasks == null) System.out.println("[FAIL]");
        else {
            tasks.clear();
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeAllByProjectName() {
        System.out.println("[REMOVE ALL TASKS FROM PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        final String name = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.removeAllByProjectName(name);
        if (tasks == null) System.out.println("[FAIL]");
        else {
            tasks.clear();
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeAllByProjectIndex() {
        System.out.println("[REMOVE ALL TASKS FROM PROJECT]");
        System.out.println("ENTER PROJECT INDEX");
        final Integer index = TerminalUtil.nextNumber() -1;
        final List<Task> tasks = projectTaskService.removeAllByProjectIndex(index);
        if (tasks == null) System.out.println("[FAIL]");
        else {
            tasks.clear();
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectTaskService.removeProjectByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        System.out.println("[OK]");
    }

}
